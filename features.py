from skimage.feature import hog

from utils import rgb2gray


def extract_hog(img):
    gray = rgb2gray(img)

    return hog(
        image=gray,
        orientations=8,
        pixels_per_cell=(8, 8),
        cells_per_block=(2, 2),
        block_norm='L2-Hys',
        visualize=False,
        transform_sqrt=True
    )


def cnn_codes():
    pass


# features_training = np.array( [ftrs_training[i].flatten() for i in range(10000)] )
# features_testing  = np.array( [ftrs_testing[i].flatten()  for i in range(10000 )] )
#
# np.savez_compressed("CIFAR10_{}-keras_features.npz".format('selected_network'), \
#                     features_training=features_training, \
#                     features_testing=features_testing,   \
#                     labels_training=y_train,             \
#                     labels_testing=y_test)
